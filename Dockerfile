# We will still use a python base image as we will run python code in order to create a database
FROM python:3.8-buster

ARG PIP_NO_CACHE_DIR=on
WORKDIR /news20js

# Expose environment variables relative to packages inside docker image
ENV MODEL_DIR="/news20js/client-tf/static/assets"
ENV DATABASE="/news20js/client-tf/static/assets/news20.sqlite"

# Install nodejs 14, demo web application and obtain pynews python package
RUN apt-get update && \
    curl -sL https://deb.nodesource.com/setup_14.x | bash - && \
    apt-get update && \
    ACCEPT_EULA=Y apt-get install -y nodejs && \
    apt-get clean && \
    npm install -g yarn && \
    python -m pip install --upgrade pip setuptools && \
    git clone https://gitlab.com/mobileml/client-tf.git && \
    git clone https://gitlab.com/mobileml/pynews.git

# First thing first - create a database
RUN cd pynews && \
    pip install --use-feature=2020-resolver .[dataprep] && \
    python -m pynews.news_to_sqlite && \
    mkdir -p ${MODEL_DIR} && \
    mv news20.sqlite ${MODEL_DIR} && \
    cd ..

# Install dependencies for the demo application
# Depending on your docker setup, it may fail if you do it during container run
RUN cd client-tf && \
    npm install && \
    npm cache clean --force && \
    cd ..

# Install dependencies for the training package
COPY package.json yarn.lock ./
RUN yarn && \
    yarn cache clean

# copy the source code into docker
COPY . ./

EXPOSE 80
CMD set -e && \
    cp .env.example .env && \
    yarn train && \
    cd client-tf && \
    node server.js
