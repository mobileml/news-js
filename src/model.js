import tf from "@tensorflow/tfjs-node";
import DataFrame from "dataframe-js";

import { cleanText, makeSequences, padSequences } from "./processing.js";


// equivalent of argsort
export const dsu = (arr1, arr2) =>
  arr1
    .map((item, index) => [arr2[index], item]) // add the args to sort by
    .sort(([arg1], [arg2]) => arg2 - arg1) // sort by the args
    .map(([, item]) => item); // extract the sorted items

export function createModel(
  wordsVocabSize,
  outputVocabSize,
  inputLength,
  embeddingDim = 64
) {
  const model = tf.sequential({
    layers: [
      tf.layers.embedding({
        inputDim: wordsVocabSize,
        outputDim: embeddingDim,
        inputLength: inputLength,
      }),
      tf.layers.globalAveragePooling1d(),
      tf.layers.dense({
        activation: "relu",
        units: embeddingDim * 2,
      }),
      tf.layers.dense({
        activation: "softmax",
        units: outputVocabSize,
      }),
    ],
  });
  model.compile({
    optimizer: tf.train.adam(),
    loss: "sparseCategoricalCrossentropy",
    metrics: ["acc"],
  });
  return model;
}

export async function predict(modelObj, inputStr, count = 5) {
  const getKey = (obj, val) => Object.keys(obj).find((key) => obj[key] === val); // For getting tags by tagid

  let df = new DataFrame({ X: [inputStr] });

  const model = modelObj["model"];
  const wordsVocab = modelObj["wordsVocab"];
  const tagsVocab = modelObj["tagsVocab"];

  // Convert input text to words
  df = df.withColumn("Words", (row) => cleanText(row.get("X")).split(" "));

  // Model works on sequences of MAX_SEQUENCE_LENGTH words, make one
  const preSeq = makeSequences(df, "Words", wordsVocab);
  const sequences = padSequences(preSeq);
  // convert JS array to TF tensor
  let tensor = tf.tensor(sequences[0]).expandDims(0);
  // run prediction, obtain a tensor
  let predictOut = await model.predict(tensor);

  const numClasses = predictOut.shape[1];
  // array of sequential indices into predictions
  const indices = Array.from(Array(numClasses), (x, index) => index);
  // convert tensor to JS array
  let predictions = await predictOut.array();
  // we predicted just a single instance, get it's results
  predictions = predictions[0];
  // prediction indices sorted by score (probability)
  const sortedIndices = dsu(indices, predictions);
  const topN = sortedIndices.slice(0, count);

  const results = topN.map(function (tagId) {
    const tag = getKey(tagsVocab, tagId);
    const prob = predictions[tagId];
    return {
      category: tag,
      score: prob,
    };
  });

  return results;
}
