import fs from "fs";

export const MAX_SEQUENCE_LENGTH = 150;
export const PAD_ID = 0;
export const UNK_ID = 1;

export function cleanText(text) {
  text = text.toLowerCase();

  // remove hyperlinks
  text = text.replace(
    /(www|https?:\/\/)(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*(),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+/g,
    ""
  );

  // replace all punctuations with ' '
  text = text.replace(/['!"#$%&\\'()*+,\-.\/:;<=>?@\[\\\]\^_`{|}~']/g, " ");

  // replace all pure numbers (int and float) with num
  text = text.replace(/\b\d+[.,]*\b/g, "num");

  // remove all non-standard symbols/characters
  text = text.replace(/[^A-Za-z0-9,.?]/g, " ");

  // remove all single letters
  text = text.replace(/\b[\w]\b/g, "");

  // replace two or more spaces by a single space
  text = text.replace(/\s\s+/g, " ");

  // replace all digit sequences by a single digit
  text = text.replace(/\d+/g, "1");

  text = text.trim();

  return text;
}

export function makeVocab(df, column, useSpecial = true) {
  let vocab = {};
  let idd = 0;
  if (useSpecial) {
    vocab = { "<PAD>": PAD_ID, "<UNK>": UNK_ID };
    idd = Math.max(...[PAD_ID, UNK_ID]) + 1;
  }
  df.chain((row) =>
    row.get(column).forEach((word) => {
      if (!vocab.hasOwnProperty(word)) {
        vocab[word] = idd;
        idd = idd + 1;
      }
    })
  );
  return vocab;
}

export function makeSequences(df, column, vocab) {
  let sequences = Array();
  df.map(function (row) {
    let wordsArray = row.get(column);
    let sequence = Array();
    wordsArray.forEach(function (word) {
      let id = vocab[word];
      if (id == undefined) {
        sequence.push(vocab["<UNK>"]);
      } else {
        sequence.push(id);
      }
    });
    sequences.push(sequence);
  });

  return sequences;
}

export function padSequences(
  sequences,
  padValue = PAD_ID,
  maxLength = MAX_SEQUENCE_LENGTH
) {
  return sequences.map(function (sequence) {
    sequence = sequence.slice(0, maxLength);
    if (sequence.length < maxLength) {
      let pad_array = Array(maxLength - sequence.length);
      pad_array.fill(padValue);
      sequence = sequence.concat(pad_array);
    }
    return sequence;
  });
}

export const storeData = (path, data) => {
  try {
    fs.writeFileSync(path, JSON.stringify(data, null, 2));
  } catch (err) {
    console.error(err);
  }
};

export const loadData = (path) => {
  try {
    return fs.readFileSync(path, "utf8");
  } catch (err) {
    console.error(err);
    return false;
  }
};
