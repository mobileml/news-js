import tf from "@tensorflow/tfjs-node";
import sqlite from "better-sqlite3";
import Pipeline from "pipeline-js";
import fs from "fs";
import DataFrame from "dataframe-js";
import strftime from "strftime";

import { createModel, dsu } from "./model.js";
import {
  cleanText,
  makeVocab,
  makeSequences,
  padSequences,
  storeData,
  MAX_SEQUENCE_LENGTH,
} from "./processing.js";

/**
 * Get a subfolder inside build directory.
 *
 * This function returns the path and creates the folder if it doesn't exist.
 *
 * @param {string} subfolder
 */
function getBuildFolder(subfolder = "") {
  const dir =
    subfolder !== ""
      ? `${process.env.BUILD_DIR}/${subfolder}`
      : process.env.BUILD_DIR;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
  }
  return dir;
}

/**
 * Make build directory
 *
 * @param {string} dir Build directory path.
 */
function makeBuildDir(dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
    console.debug(`Making directory ${dir}`);
  }
}

/**
 * Save ML features in a JSON file.
 *
 * This is an example implementation.
 *
 * @param {string} file
 * @param {*} X
 * @param {*} y
 */
function saveFeatures(file, X, y) {
  const df = new DataFrame.DataFrame({
    X: X,
    y: y,
  });
  df.toJSON(true, file);
}

// Mean value of array values
function mean(array) {
  return array.reduce((a, b) => a + b) / array.length;
}

/**
 * Fetch raw data from the database.
 *
 * The function relies on global environment variables (accessed via process)
 */
async function fetchRawData() {
  try {
    let query = `SELECT * FROM ${process.env.TABLE_NAME} WHERE year >= ?`;
    console.log(`Executing query: ${query}`);
    const db = sqlite(process.env.DATABASE);

    let records = db.prepare(query).all(process.env.CUTOFF_YEAR);
    console.log(`Got ${records.length} records`);

    return records;
  } catch (err) {
    console.log(`Error fetching the data: ${err}`);
    process.exit(1);
  }
}

function preprocessData(rawData) {
  const columns = ["Data", "Category", "Year"];
  let df = new DataFrame.DataFrame(rawData, columns);
  df.show(3);

  console.debug(`Original shape of DataFrame: ${df.dim()}`);
  df = df.dropMissingValues(["Data", "Category"]);
  df = df.filter((row) => row.get("Category") !== " ");
  console.debug(
    `Shape of DataFrame after removal of entries with problematic whitespace or NaN: ${df.dim()}`
  );

  // create train/test data split
  df = df.withColumn("testData", () => false);
  df = df.map((row) =>
    row.set("testData", row.get("Year") > process.env.TESTS_SPLIT_YEAR)
  );

  df = df.withColumn("Words", (row) => cleanText(row.get("Data")).split(" "));
  df = df.chain((row) => row.set("Category", row.get("Category").split(" ")));

  const featuresFile = `${getBuildFolder("data")}/features.json`;
  df.select("Category", "testData", "Words").toJSON(true, featuresFile);
  console.log(`Features saved to: ${featuresFile}`);

  return df;
}

function makeFeatures(baseFeatures) {
  let trainData = baseFeatures.chain((row) => row.get("testData") == false);
  let testData = baseFeatures.chain((row) => row.get("testData") == true);
  console.log(
    `Dim train ${trainData.dim()}, dim validation: ${testData.dim()}`
  );

  // prepare example entries used in a demo web application
  const exampleData = testData.sample(0.1).select("Data");

  // make vocabulary from input words and from categories we are going to predict
  let wordsVocab = makeVocab(trainData, "Words");
  let tagsVocab = makeVocab(trainData, "Category", false);

  const numWords = Object.keys(wordsVocab).length;
  const numTags = Object.keys(tagsVocab).length;
  console.log(`Words in vocabulary: ${numWords}, num categories: ${numTags}`);

  console.log("== making sequences for words in training set");
  let trainDataSequences = makeSequences(trainData, "Words", wordsVocab);
  console.log("== making sequences for words in test set");
  let testDataSequences = makeSequences(testData, "Words", wordsVocab);

  let trainTagsSequences = makeSequences(trainData, "Category", tagsVocab);
  let testTagsSequences = makeSequences(testData, "Category", tagsVocab);

  // Make all sequences the same length
  trainDataSequences = padSequences(trainDataSequences);
  testDataSequences = padSequences(testDataSequences);

  console.log(
    `First training [X, y]: ${trainDataSequences[0]} => ${trainTagsSequences[0]}`
  );

  // Optionally, save train/test features
  // saveFeatures(`${getBuildFolder('data')}/train.json`, trainDataSequences, trainTagsSequences);
  // saveFeatures(`${getBuildFolder('data')}/test.json`, testDataSequences, testTagsSequences);
  storeData(`${getBuildFolder()}/wordsVocab.json`, wordsVocab);
  storeData(`${getBuildFolder()}/tagsVocab.json`, tagsVocab);

  return {
    train: {
      X: trainDataSequences,
      y: trainTagsSequences,
    },
    test: {
      X: testDataSequences,
      y: testTagsSequences,
    },
    examples: exampleData,
    vocabs: {
      words: wordsVocab,
      tags: tagsVocab,
    },
  };
}

async function trainModel(datasetsObj) {
  const wordsVocab = datasetsObj["vocabs"]["words"];
  const tagsVocab = datasetsObj["vocabs"]["tags"];
  const numWords = Object.keys(wordsVocab).length;
  const numTags = Object.keys(tagsVocab).length;
  const model = createModel(numWords, numTags, MAX_SEQUENCE_LENGTH, 32);
  model.summary();

  // Converting data to tensors
  const xs = tf.tensor2d(datasetsObj["train"]["X"]);
  const xy = tf.tensor(datasetsObj["train"]["y"]);
  const xsVal = tf.tensor2d(datasetsObj["test"]["X"]);
  const xyVal = tf.tensor(datasetsObj["test"]["y"]);

  const epochs = Number.parseInt(process.env.EPOCHS, 10);
  const batchSize = Number.parseInt(process.env.BATCH_SIZE, 10);
  await model.fit(xs, xy, {
    epochs: epochs,
    batchSize: batchSize,
    shuffle: true,
    validationData: [xsVal, xyVal],
    callbacks: [
      tf.callbacks.earlyStopping({
        monitor: "val_acc",
        patience: 5,
        minDelta: 0.001,
      }),
      tf.node.tensorBoard(getBuildFolder("tensorboard")),
    ],
  });

  if (!fs.existsSync(process.env.MODEL_DIR)) {
    fs.mkdirSync(process.env.MODEL_DIR);
  }
  await model.save(`file://${process.env.MODEL_DIR}`);
  datasetsObj["model"] = model;

  // generate constants.js with vocabularies
  let exampleData = {};
  datasetsObj["examples"]
    .toArray()
    .forEach((el, idx) => (exampleData[idx] = el));

  let vocabs_content = `const WORDS_VOCAB = ${JSON.stringify(
    wordsVocab,
    null,
    2
  )};\n`.concat(
    `const TAGS_VOCAB = ${JSON.stringify(tagsVocab, null, 2)};\n`,
    `const EXAMPLE_DATA = ${JSON.stringify(exampleData, null, 2)};\n`,
    `const MAX_SEQUENCE_LENGTH = ${MAX_SEQUENCE_LENGTH};`
  );

  const constants_file = `${process.env.MODEL_DIR}/constants.js`;
  fs.writeFile(constants_file, vocabs_content, function (err) {
    if (err) {
      return console.log(`Failed to write constants.js file, error: ${err}`);
    }
    console.log(`Created ${constants_file}`);
  });

  const test = datasetsObj["test"];
  return {
    testData: test,
    model: model,
  };
}

let evaluateModel = async function (inputObj) {
  const testData = inputObj["testData"];
  const model = inputObj["model"];

  const allK = [1, 3, 5];
  const X = tf.tensor2d(testData["X"]);
  let predictOut = model.predict(X);

  const numClasses = predictOut.shape[1];
  // array of sequential indices into predictions
  const indices = Array.from(Array(numClasses), (x, index) => index);
  // convert tensor to JS array
  let predictions = await predictOut.array();

  let df = new DataFrame.DataFrame({
    Predictions: predictions,
    ClassIndex: testData["y"],
  });

  let recallAtK = {};
  allK.forEach((k) => {
    recallAtK[k] = [];
    df.chain((row) => {
      const predictions = row.get("Predictions");
      // prediction indices sorted by score (probability)
      const sortedIndices = dsu(indices, predictions);
      const topKClassIndices = sortedIndices.slice(0, k);
      const trueClass = row.get("ClassIndex")[0];

      recallAtK[k].push(topKClassIndices.includes(trueClass));
    });
  });

  let mlscores = [];
  const timestamp = strftime("%Y-%m-%dT%H:%M:%SZ");

  Object.keys(recallAtK).forEach(function (key) {
    const recall = mean(recallAtK[key]);
    recallAtK[key] = recall;
    const mlscore = {
      DataSet: "News20",
      Time: timestamp,
      Label: `Recall@${key}`,
      Value: recall,
    };
    mlscores.push(mlscore);
  });
  console.log("Recall at different K:");
  console.log(recallAtK);
  storeData(`${getBuildFolder()}/news-js.mlscore`, mlscores);

  return recallAtK;
};

var pipeline = new Pipeline([
  makeBuildDir,
  fetchRawData,
  preprocessData,
  makeFeatures,
  trainModel,
  evaluateModel,
]);

pipeline.process(process.env.BUILD_DIR);
