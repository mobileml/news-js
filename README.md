# Text classification pipeline in Node.js (Python vs Node.js series)

[Python vs Node.js series](https://gitlab.com/mobileml) demonstrates the differences between training a machine learning model in different languages. The end goal
is to serve the trained model in a browser. This repository contains javascript implementation.

The project is to perform text classification on a subset (eight categories) of news20 dataset. The code doesn't focus on
the model itself, but rather on the pipeline around it. The motivation behind this approach is to go one step further from the
basic how-to examples where the data is nicely prepared in two files. _Note that there are still some things one would normally do before taking such code to production._

This project reads data from a database (sqlite is used for simplicity) with an additional filtering on the database side.
The model is evaluated with metric recall@n, which again simulates a real use scenario.

Database contains a single table NEWS:

```
CREATE TABLE IF NOT EXISTS "NEWS" (
  "Data" TEXT,
  "Category" TEXT,
  "Year" INTEGER
);
```

where `Data` is post's data, `Category` is post's topic and `Year` is an artificial random year. We use `year` to separate training data from the test data.

## Docker

The docker image trains the model and runs a simple website that demonstrates model usage.

```shell
# Build image
docker build -t tfjs_demo .
# Run it. After training is done, visit http://localhost:5000/
docker run --rm -itp 5000:80 tfjs_demo

# If you want to preserve built artefacts (not the model)
# the model and the database could be obtained through web interface from http://localhost:5000/assets
docker run --rm -itv <full_path_to_local_dir>:/news20js/builds -p 5000:80 tfjs_demo
```

Read further for local machine development.

## Installation

Prerequisites:

- Node.js with yarn. Python 3.6+ with pip 19+ in order to prepare the database.

### Database creation

We have to create the databse with our dataset that we will use afterwards. This is a separate step, not related to the main code of model creation.

Install `pynews` package in editable mode:

```shell
git clone https://gitlab.com/mobileml/pynews.git
cd pynews && pip install -e .[dataprep] && cd ..
python -m pynews.news_to_sqlite
```

This will create file `news20.sqlite` in the current directory. After you have the database, you may proceed with the further steps.

### Development

```
yarn
```

## Usage

### Training

To start the training process:

```
yarn train
```

The `.env.example` file may be used to customize the training process. Copy this file to `.env` and make any necessary changes.

### Prediction

The project focuses on prediction from a browser. See docker file and [client-tf](https://gitlab.com/mobileml/client-tf) for the insights about how to predict data.
